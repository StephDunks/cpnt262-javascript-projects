

    	function loadProvinces() {
    		//instantiate my load provinces array
    		var provArray = ["Alberta", "British Columbia", "Manitoba", "New Brunswick", "Newfoundland", "Nova Scotia", "Northwest Territories", "Ontario", "Quebec", "Yukon", "Prince Edward Island"];

    		var selectBox = document.getElementById('cboProv');
    		var defaultOpt = document.createElement('option');
    		defaultOpt.value = "";
    		defaultOpt.innerHTML="-Select-";
    		selectBox.appendChild(defaultOpt);

			// this for loop will populate the options in our provinces select box
			for(index = 0; index<provArray.length; index++) {
			
				var opt = document.createElement('option');
				opt.value = provArray[index];
				opt.innerHTML = provArray[index];
				selectBox.appendChild(opt);
			}

		}


				function validateForm() {
					// this will check if the user has filled out the form correctly
					var selectBox = document.getElementById('cboProv');
					var nameBox = document.getElementById('txtName');
					var emailBox = document.getElementById('txtEmail');
					if (selectBox.selectedIndex == 0) {
						
						alert("please select a province");
						
						selectBox.focus();
						// kick user out of validation so the can fix the error
						return;

					} else if (nameBox.value == "") {
						alert ("please enter name");
						nameBox.focus();
						return;
					 	
					} else if (emailBox.value == "") {
						alert ("please enter email");
						emailBox.focus();
						return;

					} else {
						alert("You entered the form correctly!");
					}
				}
		 
