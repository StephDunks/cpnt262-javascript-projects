
//  Great work here, Stephan.  Your application
// looks and works great.  You've got everything
// named correctly as well.  The only thing I
// missed was the HTML comments...
// 19/20

// instantiate my variables
var gamePieces;
var userChoice;
var computerChoice;



function getRandomGamePiece(gamePiecesLength){
    // get a random number based off of our array's index
    var rnd = Math.floor((Math.random() * gamePiecesLength)+0);
    // return our random index
    return rnd;
  }

function startGame(){


    //get choice from user
   userChoice = prompt("Please enter: Rock, Paper, Scissors or Dynamite").toUpperCase();
   gamePieces = ["ROCK", "PAPER", "SCISSORS", "DYNAMITE"]


	// get choice from computer
   computerChoice = gamePieces[getRandomGamePiece(gamePieces.length)];
   whoWins();


	    function whoWins() {

		var results = "";

			if (userChoice === computerChoice) {
				results = "The result is a tie!";
			} else if (userChoice === "ROCK" && computerChoice === "PAPER") {
				results = "Paper Wins!";
			} else if (userChoice === "ROCK" && computerChoice === "DYNAMITE") {
				results = "Dynamite Wins!";
			} else if (userChoice === "ROCK" && computerChoice === "SCISSORS") {
				results = "Rock Wins!";
			} else if (userChoice === "PAPER" && computerChoice === "DYNAMITE") {
				results = "Dynamite Wins!";
			} else if (userChoice === "PAPER" && computerChoice === "SCISSORS") {
				results = "Scissors Wins!";
			} else if (userChoice === "PAPER" && computerChoice === "ROCK") {
				results = "Paper Wins!";
			} else if (userChoice === "SCISSORS" && computerChoice === "ROCK"){
				results = "Rock Wins!"
			} else if (userChoice === "SCISSORS" && computerChoice === "DYNAMITE"){
				results = "Scissors Wins!"
			} else if (userChoice === "SCISSORS" && computerChoice === "PAPER"){
				results = "Scissors Wins!"
			} else if (userChoice === "DYNAMITE" && computerChoice === "ROCK"){
				results = "Dynamite Wins!"
			} else if (userChoice === "DYNAMITE" && computerChoice === "SCISSORS"){
				results = "Scissors Wins!"
			} else if (userChoice === "DYNAMITE" && computerChoice === "PAPER"){
				results = "Dynamite Wins!"
			}  else results = "Not a valid entry!"

			$("#results").html(userChoice + " VS " + computerChoice + "<br/>" + results);



	   };


 }
